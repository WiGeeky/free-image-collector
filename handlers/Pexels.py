import os
import requests
import re

class Pexels:
    BASE_URL = "https://api.pexels.com/v1/"
    def __init__(self, key: str = None):
        if (key is None or key == '') and "PEXELS_API_KEY" not in os.environ:
            raise ValueError("Api Key for Pexels is not set!")
        elif key is None or key == '':
            key = os.environ.get("PEXELS_API_KEY")
        self.key = key

    def get_image_url_from_full_url(self, url: str):
        image_id = self.get_id_from_full_url(url)
        if not image_id:
            return None
        response = requests.get(self.BASE_URL + "photos/" + image_id, headers={"Authorization": self.key})
        if not response:
            print(f"Response failed with status code {response.status_code}")
            return None
        return response.json()['src']['large']

    def get_id_from_full_url(self, url: str):
        match = re.search(r"(\d+)\/?$", url)
        return None if not match else match.group(1)
