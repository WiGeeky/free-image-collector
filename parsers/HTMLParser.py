from bs4 import BeautifulSoup

class HTMLParser:
    source: str
    bs4 = None

    def __init__(self, source: str):
        self.source = source

    def get_bs4(self):
        return self.bs4 if self.bs4 else BeautifulSoup(self.source, 'html.parser')
