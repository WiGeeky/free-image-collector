from .HTMLParser import HTMLParser
class OpenGraphParser(HTMLParser):
    def __init__(self, source: str):
        super().__init__(source)

    def get_image_url(self):
        tag = self.get_bs4().find('meta', attrs={"name":"og:image"})
        tag = tag if tag else  self.get_bs4().find('meta', attrs={"property":"og:image"})
        return tag['content'] if tag else None
