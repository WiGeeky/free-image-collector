import argparse
import json

parser = argparse.ArgumentParser(description="Convert the resulting json file to a csv file containing only the url")
parser.add_argument('result_file', type=str, help="File to read the results from")
parser.add_argument('output_file', type=str, help="File to write the results to")

if __name__ == '__main__':
    args = parser.parse_args()
    with open(args.result_file, 'r') as result_file:
        results = json.load(result_file)
    urls = [url for url in results.values()]

    with open(args.output_file, 'w+') as output_file:
        for url in urls:
            output_file.write(url + "\n")
    print("Done")
