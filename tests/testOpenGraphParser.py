import unittest
import requests
from parsers import OpenGraphParser

class TestOpenGraphParser(unittest.TestCase):
    def test_get_image_url(self):
        url = "https://example.com/some-image"
        assert OpenGraphParser('<a></a><meta name="og:image" content="' + url + '">').get_image_url() == url
    def test_get_image_url_without_tag_present(self):
        assert OpenGraphParser('<a href="https://example.com">Hey there</a>').get_image_url() is None
if __name__ == '__main__':
    unittest.main()
