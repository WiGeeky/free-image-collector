# Free Image Collector
## Collect images from popular image websites, right from your bookmarks!
This project, written in Python, was made due to me being too lazy to perform "save as" on about 800
images, instead of doing that, I collected their urls, put them into a CSV file and wrote a python 
program to do the downloading.

### Supported Websites
Currently, the following websites are officially supported, you may also collect images from any website with the `og:image` tag present on its pages:
- Wikimedia Commons (`commons` - Supported through OpenGraph)
- Pixabay (`pixabay`)
- Pexels (`pexels`)
- Unsplash (`unsplash` - Supported through OpenGraph)
- Public Domain Pictures (`pdp` - Support through OpenGraph)
Any projects that I might've missed? I'm open to merge requests and issues!

### Input File
When initially starting the program, it will ask you for a csv file present in the input folder.

Let's imagine you have urls for photos of rhinos, you will need to place them in a csv file like this:
```csv
category,url
commons,https://commons.wikimedia.org/
```

Note that the category for each url is the "slug" for that website. if you manage to have the time, there could 
probably be a solution to use the fully-qualified-domain-name instead using the url.

### Setup
This project was written in Python 3.8, although it will probably run fine on newer versions. 
To set up the project, you can use the following commands:
```shell
git clone https://gitlab.com/WiGeeky/free-image-collector.git
cd free-image-collector
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
mkdir data results
cp .env.example .env
```
This will set-up the project and copy the sample environment file to `.env`.

#### Environment Variables
Once the project is set-up, you will need to set-up the environment, open the .env file using an editor or set the
variables as os environment variables. The following list explains what each api key does and where to obtain it:

- `OWNER_EMAIL` your personal email, this is used inside the user agent when sending requests to make sure websites can contact you in case of issues.
- `PIXABAY_API_KEY` you may obtain the api key [here](https://pixabay.com/api/docs/). (You will see the api-key prefilled if you're logged-in or registered already)
- `PEXELS_API_KEY` you may obtain the api key [here](https://www.pexels.com/api/new/)

### Usage
After the setup, you may use the `main.py` command to parse a csv file complient with the input file mentioned above. 
```shell
python3 main.py <source csv file> <destination json file>
```
the output is a json-file with keys being the page urls and the values being the urls for download. This was done 
for adding resumability and recovery later on.

You may process this file further as you wish, but since one of the most common usages is sending it through a downloader
like aria2, a script is available in the project for that:
```shell
python3 json_to_text.py <source json file> <destination text file>
```
the output will be a simple text file with one url on each row.


### Contributing
I'm welcoming any and all contributions to this project, whether an issue or a merge request.

### License
This project is licensed under the MIT license, see LICENSE for more information. 
Wikimedia, Pixabay, Unsplash, and Pexels are trademarks of their respective owners. 
