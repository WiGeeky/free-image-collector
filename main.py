
import argparse
import csv
import requests
import json
import os
from dotenv import load_dotenv
load_dotenv()

from parsers import OpenGraphParser
from handlers import Pexels, Pixabay

parser = argparse.ArgumentParser(description="Process a csv file and collect images")
parser.add_argument("file", type=str, help="path to the csv file")
parser.add_argument("output", type=str, help="path to the output json file")

headers = {
    'User-Agent': "Free Image Collector v0.1.0 +https://gitlab.com/WiGeeky/free-image-collector, Run By <" + os.environ.get('OWNER_EMAIL') + ">",
    'From': os.environ.get('OWNER_EMAIL'),
}

def get_url(row: str):
    response = requests.get(row[1], headers=headers)
    if not response:
        print(f"Request to {row[0]} failed with status code {response.status_code}")
    return None if not response else OpenGraphParser(response.text).get_image_url()

if __name__ == '__main__':
    args = parser.parse_args()
    input_file = open(args.file, newline='')
    results = {}

    pexels = Pexels()
    pixabay = Pixabay()

    reader = csv.reader(input_file)
    i = 0
    for row in reader:
        if i == 0:
            i+= 1
            continue
        if row[0] == 'pixabay':
            url = pixabay.get_image_url_from_full_url(row[1])
        elif row[0] == 'pexels':
            url = pexels.get_image_url_from_full_url(row[1])
        else:
            url = get_url(row)

        if url:
            results[row[1]] = url
        else:
            print(f"Something went wrong while trying to process {row[1]}")

        i += 1
        if i % 50 == 0:
            print(f"Reached the {i}th item!")

    input_file.close()

    with open(args.output, 'w+') as output_file:
        json.dump(results, output_file, indent=4)
    print("Processing has finished successfully")
